from flask import Flask, render_template,jsonify, json
data_app=Flask(__name__)
#Names

@data_app.route('/names')
def data_name():
    with open('details.json') as f:
        Data = json.load(f)
    for data in Data['details']:
        del data['ID']
    return jsonify(Data['details'])
#ID
@data_app.route('/id')
def data_id():
    with open('details.json') as f:
        Data = json.load(f)
    for data in Data['details']:
        del data['Name']
    return jsonify(Data['details'])
#All
@data_app.route('/all')
def data_all():
    with open('details.json') as f:
        Data = json.load(f)
    return jsonify(Data['details'])

if __name__=='__main__':
    data_app.run(debug=True,host='172.18.0.3',port=5001)