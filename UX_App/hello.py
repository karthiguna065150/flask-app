from flask import Flask, render_template,request
import json, requests
import flask
app=Flask(__name__)


@app.route("/")
def hello():
    return render_template("home.html",title='Home')

@app.route("/results", methods=['POST'])
def result():
    #ip = flask.request.remote_addr
    host_1= "http://172.18.0.3:5001/"
    if request.method == 'POST':
        if request.form['result'] == 'name':
             name =requests.get(host_1+'names').json()
             return render_template('data_process.html', name =name , value='name', title='Results - Name')
             
        if request.form['result'] == 'id':
            id = requests.get(host_1+'id').json()
            return render_template('data_process.html', id =id , value='id', title='Results - ID')

        if request.form['result'] == 'all':
            all= requests.get(host_1+'all').json()
            return render_template('data_process.html', all =all , value='all', title='Results - All')



if __name__=='__main__':
    app.run(debug=True,host ='172.18.0.2',port=5000)